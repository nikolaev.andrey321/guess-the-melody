import random


def phrase(arr_words):
    """
    Функция для добавления вариативности слов Алисы

    Parameters:
        arr_words(list): список синонимычных фраз

    Returns:
        (str): одна из предложенных фраз
    """

    return random.choice(arr_words)

