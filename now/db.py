# import pymysql
from os import name
import sqlite3
import random

# const minus = 0
# const plus = 1
# melodies = {1 : [minus, plus, words, autor], 2: }
# melodies[1][1]

def pick_song(difficult, janr):
    """
    Функция работаюная с базой данных

    Parameters:
        column(str): название столбца базы данных
        numb(int): id строки

    Returns:
        i[0](str): необходимые данные с бд
    """
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()   
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            rand = random.randint(0, length_data)
            return rand
        #     cur.execute(f"UPDATE melodies SET flag = 0 WHERE id = {data[rand][0]}")

# def random_for_play(data):
#     length_data = len(data) - 1
#     if length_data != -1:
#         rand = random.randint(0, length_data)
#         return rand
def data_base(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()   
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[rand][1]
        

def rigth_song(rand, difficult, janr):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            cur.execute(f"UPDATE melodies SET flag = 0 WHERE id = {data[rand][0]}")


def name_song(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[rand][3].lower()


def plus_song(id):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE id = {id}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[0][2]

def help_song(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[rand][4]

def text_song(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[rand][8].lower()

def length_song(difficult, janr):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()   
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        return length_data

def reset_janr_song(difficult, janr):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 0 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            cur.execute(f"UPDATE melodies SET flag = 1")

def id_song(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[rand][0]

def answer_song(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            answer = 'Ответ: ' + data[rand][4] + ' ' + data[rand][3]
            return answer

def author_song(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[rand][4].lower()

def author_song_rus(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[rand][10].lower()

def name_rus(difficult, janr, rand):
    with sqlite3.connect('server.db') as con:
        cur = con.cursor()
        cur.execute(f"SELECT * FROM melodies WHERE janr = {janr} AND flag = 1 AND difficult = {difficult}")
        data = cur.fetchall()
        length_data = len(data) - 1
        if length_data != -1:
            return data[rand][9].lower()

# rand = pick_song(1, 2)
# if (name_song(1,2, rand) in 'незабудка dasdas dasdasdasads'):
#     print("сос")
# rand = pick_song(1, 2)
# print(data_base(1, 2, rand)
