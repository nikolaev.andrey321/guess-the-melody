# coding: utf-8
# Импортирует поддержку UTF-8.
from __future__ import unicode_literals

# Импортируем модули для работы с JSON и логами.э



import json

import logging

from flask.globals import session
import db
from dialogue import phrase

# Импортируем подмодули Flask для запуска веб-сервиса.
from flask import Flask, request
app = Flask(__name__)

logging.basicConfig(level=logging.DEBUG)


# Хранилище данных о сессиях.
sessionStorage = {}

# Задаем параметры приложения Flask.
@app.route("/", methods=['POST'])

def main():
# Функция получает тело запроса и возвращает ответ.
    logging.info('Request: %r', request.json)
    response = {
        "version": request.json['version'],
        "session": request.json['session'],
        "response": {
            "end_session": False
        }
    }
    handle_dialog(request.json, response, sessionStorage)

    logging.info('Response: %r', response)

    return json.dumps(
        response,
        ensure_ascii=False,
        indent=2
    )

# Функция для непосредственной обработки диалога.
def handle_dialog(req, res, sessionStorage):
    if req['session']['new']:
        res['response']['text'] = 'Добро пожаловать в игру Угадай Мелодию!\
            Сейчас я буду включать различные мелодии, а вы будете угадывать\
            их и петь. Ну что, начинаем?'
        res['response']['tts'] = 'Добро пожаловать в игр+у Угад+ай Мелодию!\
            Сейчас я буду включать различные мелодии, а вы будете уг+адывать\
            их и петь. Ну чт+о, начинаем?'
        sessionStorage['menu'] = 1
        sessionStorage['level'] = 0
        res['response']['buttons'] = [{}]
        res['response']['buttons'][0]['title'] = 'да'
        res['response']['buttons'][0]['hide'] = True
        return sessionStorage


    positive_res = [
            'ладно',
            'давай',
            'начинаем',
            'хорошо',
            'так уж и быть',
            'да',
            'начнем',
            'давайте',
            'стартуем',
            'погнали',
            'конечно',
            'ок',
        ]

    for i in positive_res:
        if sessionStorage['menu'] < 2 and\
            i in req['request']['original_utterance'].lower() and\
            'не ' not in req['request']['original_utterance'].lower() and\
            'нет' not in req['request']['original_utterance'].lower():
            res['response']['text'] = f"Отлично! Кстати, если\
                {phrase(['затрудняетесь', 'не получается угадать песню'])}, вы можете попросить\
                подсказку, пропустить мелодию или просто узнать ответ.\n А сейчас\
                {phrase(['выберите', 'давайте выберем'])} одну из следующих\
                сложностей: нормально, сложно. На сложном уровне, в отличие от нормально, дается меньше времени.\
                Если хотите, вы можете поменять сложность, просто попросив об этом"
                # категорий: Рэп, Классика, Из мультфильмов, Дискотека 90-х, Поп"
            sessionStorage['menu'] = 2
            res['response']['buttons'] = [{}, {}]
            res['response']['buttons'][0]['title'] = 'Сложно'
            res['response']['buttons'][0]['hide'] = True
            res['response']['buttons'][1]['title'] = 'Нормально'
            res['response']['buttons'][1]['hide'] = True
            sessionStorage['help'] = 1
            sessionStorage['levelmenu'] = 0
            sessionStorage['lenmenu'] = 0
            sessionStorage['levelmenuflag'] = 0
            return sessionStorage

    if sessionStorage['menu'] < 2:
        res['response']['text'] = f"Это конечно хорошо, но {phrase(['может', 'давайте'])} лучше начнем игру?"
        return

    if sessionStorage['levelmenuflag'] == 1 and req['request']['original_utterance'].lower() == 'сменить сложность':
        if sessionStorage['menu'] == 4:
            res['response']['text'] = 'нельзя мненять сложность во время проигрыша песни или ее угадывания'
            return
        else:        
            sessionStorage['levelmenu'] = 1
            sessionStorage['lenmenu'] = 0
            res['response']['text'] = 'Введите на какой уровень сложности вы хотите сменить'
            res['response']['buttons'] = [{}, {}]
            res['response']['buttons'][0]['title'] = 'Сложно'
            res['response']['buttons'][0]['hide'] = True
            res['response']['buttons'][1]['title'] = 'Нормально'
            res['response']['buttons'][1]['hide'] = True    
            return sessionStorage
    if sessionStorage['levelmenu'] == 1 and req['request']['original_utterance'].lower() in [
            'трудно',
            'тяжело',
            'сложно',
            'нормально',
            'норм',
            'средний',
            'нормальный',
    ]:
        if req['request']['original_utterance'].lower() in ['трудно', 'тяжело', 'сложно']:
            res['response']['text'] = 'Уровень был успешно сменен на сложный. ' + sessionStorage['previoustext']
            if 'Остаемся' in sessionStorage['previoustext']:
                res['response']['buttons'] = [{},{},{}]
                res['response']['buttons'][0]['title'] = 'Остаемся'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Сменить категорию'
                res['response']['buttons'][1]['hide'] = True
                res['response']['buttons'][2]['title'] = 'Сменить сложность'
                res['response']['buttons'][2]['hide'] = True 
            else:
                res['response']['buttons'] = [{},{},{},{},{},{},{}]
                res['response']['buttons'][0]['title'] = 'Рэп'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Классика'
                res['response']['buttons'][1]['hide'] = True
                res['response']['buttons'][2]['title'] = 'Рок'
                res['response']['buttons'][2]['hide'] = True
                res['response']['buttons'][3]['title'] = 'Дискотека 80ых-90ых'
                res['response']['buttons'][3]['hide'] = True                      
                res['response']['buttons'][4]['title'] = 'Поп'
                res['response']['buttons'][4]['hide'] = True
                res['response']['buttons'][5]['title'] = 'Аниме'
                res['response']['buttons'][5]['hide'] = True     
                res['response']['buttons'][6]['title'] = 'Сменить сложность'
                res['response']['buttons'][6]['hide'] = True                
            sessionStorage['difficult'] = 2
        elif req['request']['original_utterance'].lower() in ['нормально', 'норм', 'нормальный', 'средний']:
            res['response']['text'] = 'Уровень был успешно сменен на нормальный. ' + sessionStorage['previoustext']
            if 'Остаемся' in sessionStorage['previoustext']:
                res['response']['buttons'] = [{},{},{}]
                res['response']['buttons'][0]['title'] = 'Остаемся'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Сменить категорию'
                res['response']['buttons'][1]['hide'] = True
                res['response']['buttons'][2]['title'] = 'Сменить сложность'
                res['response']['buttons'][2]['hide'] = True 
            else:
                res['response']['buttons'] = [{},{},{},{},{},{},{}]
                res['response']['buttons'][0]['title'] = 'Рэп'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Классика'
                res['response']['buttons'][1]['hide'] = True
                res['response']['buttons'][2]['title'] = 'Рок'
                res['response']['buttons'][2]['hide'] = True
                res['response']['buttons'][3]['title'] = 'Дискотека 80ых-90ых'
                res['response']['buttons'][3]['hide'] = True                      
                res['response']['buttons'][4]['title'] = 'Поп'
                res['response']['buttons'][4]['hide'] = True
                res['response']['buttons'][5]['title'] = 'Аниме'
                res['response']['buttons'][5]['hide'] = True     
                res['response']['buttons'][6]['title'] = 'Сменить сложность'
                res['response']['buttons'][6]['hide'] = True             
            sessionStorage['difficult'] = 1
        sessionStorage['levelmenu'] = 0
        return sessionStorage
    elif sessionStorage['levelmenu'] == 1:
        res['response']['buttons'] = [{}, {}]
        res['response']['buttons'][0]['title'] = 'Сложно'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Нормально'
        res['response']['buttons'][1]['hide'] = True            
        res['response']['text'] = 'Не помню чтобы у меня был такой уровень сложности, попробуйте назвать его еще раз'
        return

    if sessionStorage['menu'] == 2 and req['request']['original_utterance'].lower() in [
            'трудно',
            'тяжело',
            'сложно',
            'нормально',
            'норм',
            'средний',
            'нормальный',
    ]:
        sessionStorage['previoustext'] = 'А сейчас выберем одну из следующих\
            категорий: Рэп, Классика, Рок, Дискотека 80ых-90ых, Поп, Аниме'
        sessionStorage['levelmenuflag'] = 1
        if req['request']['original_utterance'].lower() in ['трудно', 'тяжело', 'сложно']:
            res['response']['text'] = 'Как я погляжу вы уверены в своих силах. А сейчас выберем одну из следующих\
            категорий: Рэп, Классика, Рок, Дискотека 80ых-90ых, Поп, Аниме'
            sessionStorage['menu'] = 3
            sessionStorage['difficult'] = 2
            res['response']['buttons'] = [{},{},{},{},{},{},{}]
            res['response']['buttons'][0]['title'] = 'Рэп'
            res['response']['buttons'][0]['hide'] = True
            res['response']['buttons'][1]['title'] = 'Классика'
            res['response']['buttons'][1]['hide'] = True
            res['response']['buttons'][2]['title'] = 'Рок'
            res['response']['buttons'][2]['hide'] = True
            res['response']['buttons'][3]['title'] = 'Дискотека 80ых-90ых'
            res['response']['buttons'][3]['hide'] = True                      
            res['response']['buttons'][4]['title'] = 'Поп'
            res['response']['buttons'][4]['hide'] = True
            res['response']['buttons'][5]['title'] = 'Аниме'
            res['response']['buttons'][5]['hide'] = True        
            res['response']['buttons'][6]['title'] = 'Сменить сложность'
            res['response']['buttons'][6]['hide'] = True        
            return sessionStorage
        elif req['request']['original_utterance'].lower() in ['нормально', 'норм', 'нормальный', 'средний']:
            res['response']['text'] = 'Отличный выбор, вам будет гораздо проще чем на сложном уровне сложности. А сейчас выберем одну из следующих\
            категорий: Рэп, Классика, Рок, Дискотека 80ых-90ых, Поп, Аниме'
            sessionStorage['menu'] = 3
            sessionStorage['difficult'] = 1
            res['response']['buttons'] = [{},{},{},{},{},{},{}]
            res['response']['buttons'][0]['title'] = 'Рэп'
            res['response']['buttons'][0]['hide'] = True
            res['response']['buttons'][1]['title'] = 'Классика'
            res['response']['buttons'][1]['hide'] = True
            res['response']['buttons'][2]['title'] = 'Рок'
            res['response']['buttons'][2]['hide'] = True
            res['response']['buttons'][3]['title'] = 'Дискотека 80ых-90ых'
            res['response']['buttons'][3]['hide'] = True                      
            res['response']['buttons'][4]['title'] = 'Поп'
            res['response']['buttons'][4]['hide'] = True
            res['response']['buttons'][5]['title'] = 'Аниме'
            res['response']['buttons'][5]['hide'] = True     
            res['response']['buttons'][6]['title'] = 'Сменить сложность'
            res['response']['buttons'][6]['hide'] = True                         
            return sessionStorage
    elif sessionStorage['menu'] < 3:
        khm = ['Не помню, чтобы у меня была такая сложность', \
            'Такой сложности, к счастью или к сожалению у меня нет']
        res['response']['text'] = f"{phrase(khm)}. Так, какую сложность вы выбираете?"
        sessionStorage['menu'] == 2
        res['response']['buttons'] = [{}, {}]
        res['response']['buttons'][0]['title'] = 'Сложно'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Нормально'
        res['response']['buttons'][1]['hide'] = True
        return sessionStorage


    if sessionStorage['lenmenu'] != 0:
            if req['request']['original_utterance'].lower() in ['сменить жанр и сложность', 'перейти в другой жанр и сложность', 'смени этот жанр и сложность', 'переди в другой жанр и сложность',\
                    'сменить сложность и жанр', 'перейти в другой сложность и жанр', 'смени этот сложность и жанр', 'переди в другой сложность и жанр']:
                sessionStorage['lenmenu'] = 0
                sessionStorage['menu'] = 2
                res['response']['text'] = 'Выберите новую сложность музыки'
                res['response']['buttons'] = [{}, {}]
                res['response']['buttons'][0]['title'] = 'Сложно'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Нормально'
                res['response']['buttons'][1]['hide'] = True
                return sessionStorage
            elif req['request']['original_utterance'].lower() in ['обновить', 'обновить песни', 'обнови', 'обнови песни','обновить песни в этом жанре и сложности', 'обнови песни в этом жанре и сложности']:
                db.reset_janr_song(sessionStorage['difficult'], sessionStorage['janr'])
                sessionStorage['lenmenu'] = 0
                sessionStorage['rand'] = db.pick_song(sessionStorage['difficult'], sessionStorage['janr'])
                sessionStorage['menu'] = 4
                res['response']['text'] = 'песни были обновлены, слушайте внимательно '
                res['response']['tts'] = f"песни были обновлены, слушайте внимательно и попробуйте \
                    угадать эту мелодию {db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"
                res['response']['buttons'] = [{},{},{},{}]
                res['response']['buttons'][0]['title'] = 'Знаю ответ'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Прослушать ещё раз'
                res['response']['buttons'][1]['hide'] = True
                res['response']['buttons'][2]['title'] = 'Пропустить'
                res['response']['buttons'][2]['hide'] = True                      
                res['response']['buttons'][3]['title'] = 'Узнать ответ'
                res['response']['buttons'][3]['hide'] = True 
                if sessionStorage['help'] == 1:
                    res['response']['buttons'].append({})
                    res['response']['buttons'][4]['title'] = 'Подсказка'
                    res['response']['buttons'][4]['hide'] = True 
                return sessionStorage
            else:
                res['response']['text'] = 'Не понимаю что вы от меня хотите, попробуйте объяснить лучше'
                return


    if sessionStorage['menu'] == 3 and req['request']['original_utterance'].lower() in [
            'рок',
            'рэп',
            'классика',
            'поп',
            'аниме',
            'дискотека',
            'дискотека 80-90ых',
            'дискотека 80ых-90ых',
            '80-90ых',
            'дискотека 80-90',
            'дискотека 80 и 90',
    ]:
        
        if req['request']['original_utterance'].lower() == 'рок':
            sessionStorage['janr'] = 1
            if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
                res['response']['text'] = 'Песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?'
                sessionStorage['lenmenu'] = 1
                res['response']['buttons'] = [{},{}]
                res['response']['buttons'][0]['title'] = 'Обновить песни'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
                res['response']['buttons'][1]['hide'] = True                     
                return sessionStorage
            sessionStorage['rand'] = db.pick_song(sessionStorage['difficult'], sessionStorage['janr'])
            res['response']['text'] = 'Рок-музыка - это широкий жанр популярной музыки , который зародился как «рок-н-ролл » в Соединенных Штатах в конце 1940-х - начале 1950-х годов, '
            res['response']['tts'] = f"Любите Рок? Попробуйте\
                угадать эту мелодию {db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"


        elif 'поп' in req['request']['original_utterance'].lower():
            sessionStorage['janr'] = 2
            if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
                res['response']['text'] = 'Песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?'
                sessionStorage['lenmenu'] = 1
                res['response']['buttons'] = [{},{}]
                res['response']['buttons'][0]['title'] = 'Обновить песни'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
                res['response']['buttons'][1]['hide'] = True                  
                return sessionStorage
            sessionStorage['rand'] = db.pick_song(sessionStorage['difficult'], sessionStorage['janr'])
            res['response']['text'] = 'Любите попсу? Тогда слушаем и угадываем песню! Как только узнали, '
            res['response']['tts'] = f"Любите попсу? Тогда попробуйте\
                угадать эту мелодию {db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"

        elif req['request']['original_utterance'].lower() == 'рэп':
            sessionStorage['janr'] = 3
            if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
                res['response']['text'] = 'Песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?'
                sessionStorage['lenmenu'] = 1
                res['response']['buttons'] = [{},{}]
                res['response']['buttons'][0]['title'] = 'Обновить песни'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
                res['response']['buttons'][1]['hide'] = True                  
                return sessionStorage
            sessionStorage['rand'] = db.pick_song(sessionStorage['difficult'], sessionStorage['janr'])
            res['response']['text'] = 'Ритмичный речитатив, обычно читающийся под музыку с тяжёлым битом., '
            res['response']['tts'] = f"Слушаем и угадываем {db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"

        elif req['request']['original_utterance'].lower() == 'аниме': 
            sessionStorage['janr'] = 4
            if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
                res['response']['text'] = 'Песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?'
                sessionStorage['lenmenu'] = 1
                res['response']['buttons'] = [{},{}]
                res['response']['buttons'][0]['title'] = 'Обновить песни'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
                res['response']['buttons'][1]['hide'] = True                  
                return sessionStorage
            sessionStorage['rand'] = db.pick_song(sessionStorage['difficult'], sessionStorage['janr'])
            res['response']['text'] = f"{phrase(['Отлично', 'Хорошо', 'Хороший выбор'])}. В этой категории будут проигрываться опенинги различных аниме, название которого вы постараетесь отгадать"
            res['response']['tts'] = f"Отличный выбор. Пробуем\
                угадать название этого аниме {db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"

        elif req['request']['original_utterance'].lower() == 'классика':
            sessionStorage['janr'] = 5
            if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
                res['response']['text'] = 'Песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?'
                sessionStorage['lenmenu'] = 1
                res['response']['buttons'] = [{},{}]
                res['response']['buttons'][0]['title'] = 'Обновить песни'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
                res['response']['buttons'][1]['hide'] = True                  
                return sessionStorage
            sessionStorage['rand'] = db.pick_song(sessionStorage['difficult'], sessionStorage['janr'])
            res['response']['text'] = 'Классика - это образцовые музыкальные произведения, золотой фонд мировой музыкальной культуры, '
            res['response']['tts'] = f"Слушаете классику? Попробуйте\
                угадать эту мелодию {db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"

        elif 'дискотека' in req['request']['original_utterance'].lower() or '80' in req['request']['original_utterance'].lower() or '90' in req['request']['original_utterance'].lower():
            sessionStorage['janr'] = 6
            if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
                res['response']['text'] = 'Песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?'
                sessionStorage['lenmenu'] = 1
                res['response']['buttons'] = [{},{}]
                res['response']['buttons'][0]['title'] = 'Обновить песни'
                res['response']['buttons'][0]['hide'] = True
                res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
                res['response']['buttons'][1]['hide'] = True                  
                return sessionStorage
            sessionStorage['rand'] = db.pick_song(sessionStorage['difficult'], sessionStorage['janr'])
            res['response']['text'] = 'Слушаем песню примиком из 80-ых и 90-ых'
            res['response']['tts'] = f"Слушаем песню примиком из 80-ых и 90-ых {db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"
        res['response']['buttons'] = [{},{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Знаю ответ'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Подсказка'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Прослушать ещё раз'
        res['response']['buttons'][2]['hide'] = True
        res['response']['buttons'][3]['title'] = 'Пропустить'
        res['response']['buttons'][3]['hide'] = True                      
        res['response']['buttons'][4]['title'] = 'Узнать ответ'
        res['response']['buttons'][4]['hide'] = True 
        sessionStorage['genre'] = req['request']['original_utterance'].title()
        sessionStorage['menu'] = 4
        # if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) != -1:
        #     sessionStorage['menu'] = 4
        # else:
        #     res['response']['text'] = 'Песен, которые вы можете угадывать больше нету, перейти в другой жанр или обнулить этот жанр?'
        #     if req['request']['original_utterance'].lower() in ['сменить жанр', 'сменить', 'перейти в другой жанр', 'смени','смени этот жанр', 'переди в другой жанр']:
        #         return
        #     elif req['request']['original_utterance'].lower() in ['обнулить', 'обнулить этот жанр', 'обнули', 'обнули этот жанр']:
        #         db.reset_janr_song(sessionStorage['difficult'], sessionStorage['janr'])
        #         return
        #     else:
        #         res['response']['text'] = 'Не понимаю что вы от меня хотите, попробуйте объяснить лучше'
        #         return
        return sessionStorage

    elif sessionStorage['menu'] < 4:
        khm = ['Не помню, чтобы у меня была такая категория', \
            'Такой категории, к счастью или к сожалению у меня нет']
        res['response']['text'] = f"{phrase(khm)}. Так, какой жанр вы выбираете?"
        sessionStorage['menu'] == 3
        return sessionStorage


    if (sessionStorage['menu'] == 4) and ((db.name_song(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand']) in req['request']['original_utterance'].lower()) or
    (db.name_rus(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand']) in req['request']['original_utterance'].lower()) or
    ((req['request']['original_utterance'].lower() in db.text_song(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand']) and len(req['request']['original_utterance'].split()) >= 3))):
        sessionStorage['id'] = db.id_song(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])
        db.rigth_song(sessionStorage['rand'], sessionStorage['difficult'], sessionStorage['janr'])
        if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
            res['response']['text'] = 'Совершенно верно, однако песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?'
            sessionStorage['lenmenu'] = 1
            sessionStorage['help'] = 1
            sessionStorage['menu'] = 5
            res['response']['buttons'] = [{},{}]
            res['response']['buttons'][0]['title'] = 'Обновить песни'
            res['response']['buttons'][0]['hide'] = True
            res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
            res['response']['buttons'][1]['hide'] = True              
            return sessionStorage
        sessionStorage['previoustext'] = 'Остаемся в этой категории или выберем другую?'
        res['response']['text'] = f"{phrase(['Правильно!', 'В точку!', 'Совершенно верно!'])}\
            А теперь слушаем и танцуем вместе"
        res['response']['tts'] = f"{phrase(['Правильно!', 'В точку!', 'Совершенно верно!'])}\
            А теперь слушаем и танцуем вместе\
            {db.plus_song(sessionStorage['id'])} sil <[500]>\
            Остаемся в этой категории или выберем другую?"
        res['response']['buttons'] = [{},{},{}]
        res['response']['buttons'][0]['title'] = 'Остаемся'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Сменить категорию'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Сменить сложность'
        res['response']['buttons'][2]['hide'] = True
        sessionStorage['help'] = 1
        sessionStorage['menu'] = 5
        return sessionStorage

    elif sessionStorage['help'] == 1 and\
        req['request']['original_utterance'].lower() == 'подсказка' and sessionStorage['menu'] == 4:
        res['response']['text'] = f"Исполняет всеми известный {db.help_song(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"
        sessionStorage['help'] = 0
        res['response']['buttons'] = [{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Знаю ответ'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Прослушать ещё раз'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Пропустить'
        res['response']['buttons'][2]['hide'] = True                      
        res['response']['buttons'][3]['title'] = 'Узнать ответ'
        res['response']['buttons'][3]['hide'] = True 
        return sessionStorage

    elif req['request']['original_utterance'].lower() in ['прослушать еще раз', 'повтори', 'повторить', 'прослушать ещё раз'] and sessionStorage['menu'] == 4:
        res['response']['text'] = "Слушаем внимательней..."
        res['response']['tts'] = f"Слушаем вним+ательней...{db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"
        res['response']['buttons'] = [{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Знаю ответ'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Прослушать ещё раз'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Пропустить'
        res['response']['buttons'][2]['hide'] = True                      
        res['response']['buttons'][3]['title'] = 'Узнать ответ'
        res['response']['buttons'][3]['hide'] = True 
        if sessionStorage['help'] == 1:
            res['response']['buttons'].append({})
            res['response']['buttons'][4]['title'] = 'Подсказка'
            res['response']['buttons'][4]['hide'] = True 
        return sessionStorage

    elif req['request']['original_utterance'].lower() in ['пропустить', 'далее'] and sessionStorage['menu'] == 4:
        sessionStorage['previoustext'] = 'Остаемся в этой категории или выберем другую?'
        res['response']['text'] = 'Хорошо, эту песню пока отложим на потом.\
            Остаемся в этой категории или выберем другую?'
        sessionStorage['help'] = 1
        sessionStorage['menu'] = 5
        res['response']['buttons'] = [{},{},{}]
        res['response']['buttons'][0]['title'] = 'Остаемся'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Сменить категорию'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Сменить сложность'
        res['response']['buttons'][2]['hide'] = True               
        return sessionStorage

    elif req['request']['original_utterance'].lower() in ['узнать ответ', 'скажи ответ'] and sessionStorage['menu'] == 4:
        sessionStorage['id'] = db.id_song(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])
        sessionStorage['answer'] = db.answer_song(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])
        db.rigth_song(sessionStorage['rand'], sessionStorage['difficult'], sessionStorage['janr'])
        if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
            res['response']['text'] = f"{sessionStorage['answer']}, песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?"
            sessionStorage['lenmenu'] = 1
            sessionStorage['help'] = 1
            sessionStorage['menu'] = 5
            res['response']['buttons'] = [{},{}]
            res['response']['buttons'][0]['title'] = 'Обновить песни'
            res['response']['buttons'][0]['hide'] = True
            res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
            res['response']['buttons'][1]['hide'] = True            
            return sessionStorage
        sessionStorage['previoustext'] = 'Остаемся в этой категории или выберем другую?'
        res['response']['text'] = f"{sessionStorage['answer']}"
        res['response']['tts'] = f"Ответ\
            {db.plus_song(sessionStorage['id'])} sil <[500]>\
            Остаемся в этой категории или выберем другую?"
        res['response']['buttons'] = [{},{},{}]
        res['response']['buttons'][0]['title'] = 'Остаемся'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Сменить категорию'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Сменить сложность'
        res['response']['buttons'][2]['hide'] = True      
        sessionStorage['help'] = 1
        sessionStorage['menu'] = 5
        return sessionStorage

    elif req['request']['original_utterance'].lower() not in ["остаёмся", "остаемся"] and db.length_song(sessionStorage['difficult'], sessionStorage['janr']) != -1\
        and ((req['request']['original_utterance'].lower() in db.author_song(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])) or
        (req['request']['original_utterance'].lower() in db.author_song_rus(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand']))) and sessionStorage['menu'] == 4:
        res['response']['text'] = 'Автора вы назвали верно, попробуйте угадать песню, вы близки'
        res['response']['buttons'] = [{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Знаю ответ'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Прослушать ещё раз'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Пропустить'
        res['response']['buttons'][2]['hide'] = True                      
        res['response']['buttons'][3]['title'] = 'Узнать ответ'
        res['response']['buttons'][3]['hide'] = True 
        return sessionStorage

    elif req['request']['original_utterance'].lower() == 'знаю ответ' and sessionStorage['menu'] == 4:
        res['response']['text'] = 'Что это была за песня?'
        res['response']['buttons'] = [{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Знаю ответ'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Прослушать ещё раз'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Пропустить'
        res['response']['buttons'][2]['hide'] = True                      
        res['response']['buttons'][3]['title'] = 'Узнать ответ'
        res['response']['buttons'][3]['hide'] = True 
        if sessionStorage['help'] == 1:
            res['response']['buttons'].append({})
            res['response']['buttons'][4]['title'] = 'Подсказка'
            res['response']['buttons'][4]['hide'] = True             
        return


    # elif req['request']['original_utterance'].lower() in ['вернуться к категориям', 'сменить категорию', 'другая категория', 'назад к категориям'] and sessionStorage['menu'] == 4:
    #     res['response']['text'] = 'Выберите категорию: Рэп, Классика, Рок, Дискотека 80ых-90ых, Поп, Аниме'
    #     sessionStorage['menu'] = 3
    #     return sessionStorage

    elif sessionStorage['help'] == 1 and sessionStorage['menu'] < 5:
        res['response']['text'] = 'Если не знаете ответа вы можете пропустить песню, воспользоваться подсказкой или узнать ответ'
        res['response']['buttons'] = [{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Знаю ответ'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Прослушать ещё раз'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Пропустить'
        res['response']['buttons'][2]['hide'] = True                      
        res['response']['buttons'][3]['title'] = 'Узнать ответ'
        res['response']['buttons'][3]['hide'] = True 
        if sessionStorage['help'] == 1:
            res['response']['buttons'].append({})
            res['response']['buttons'][4]['title'] = 'Подсказка'
            res['response']['buttons'][4]['hide'] = True           
        return

    elif sessionStorage['menu'] < 5:
        res['response']['text'] = 'Ответ не тот, вы можете пропустить песню, а потом сменить сложность или категорию'
        res['response']['buttons'] = [{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Знаю ответ'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Прослушать ещё раз'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Пропустить'
        res['response']['buttons'][2]['hide'] = True                      
        res['response']['buttons'][3]['title'] = 'Узнать ответ'
        res['response']['buttons'][3]['hide'] = True         
        return


    if sessionStorage['menu'] == 5 and req['request']['original_utterance'].lower() in ['остаемся', 'остаёмся']:
        if db.length_song(sessionStorage['difficult'], sessionStorage['janr']) == -1:
            res['response']['text'] = 'Песен с такой категорией и сложностью, которые вы можете угадывать больше нету, сменить жанр и сложность или обновить песни в этом жанре и сложности?'
            sessionStorage['lenmenu'] = 1
            res['response']['buttons'] = [{},{}]
            res['response']['buttons'][0]['title'] = 'Обновить песни'
            res['response']['buttons'][0]['hide'] = True
            res['response']['buttons'][1]['title'] = 'Сменить жанр и сложность'
            res['response']['buttons'][1]['hide'] = True              
            return sessionStorage
        sessionStorage['rand'] = db.pick_song(sessionStorage['difficult'], sessionStorage['janr'])
        res['response']['text'] = f"Тогда слушаем следующую песню\
            в категории '{sessionStorage['genre']}'"
        res['response']['tts'] = f"Тогда слушаем следующую песню\
            в категории {sessionStorage['genre']} {db.data_base(sessionStorage['difficult'], sessionStorage['janr'], sessionStorage['rand'])}"
        sessionStorage['menu'] = 4
        res['response']['buttons'] = [{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Знаю ответ'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Прослушать ещё раз'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Пропустить'
        res['response']['buttons'][2]['hide'] = True                      
        res['response']['buttons'][3]['title'] = 'Узнать ответ'
        res['response']['buttons'][3]['hide'] = True 
        if sessionStorage['help'] == 1:
            res['response']['buttons'].append({})
            res['response']['buttons'][4]['title'] = 'Подсказка'
            res['response']['buttons'][4]['hide'] = True 
        return sessionStorage

    elif sessionStorage['menu'] == 5 and req['request']['original_utterance'].lower() in ['другую', 'сменить категорию', 'другую категорию', 'другая']:
        sessionStorage['menu'] = 3
        res['response']['text'] = "Какую выберем на этот раз, Рэп, Классика, Рок, Дискотека 80ых-90ых, Поп, Аниме?"
        res['response']['buttons'] = [{},{},{},{},{},{},{}]
        res['response']['buttons'][0]['title'] = 'Рэп'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Классика'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Рок'
        res['response']['buttons'][2]['hide'] = True
        res['response']['buttons'][3]['title'] = 'Дискотека 80ых-90ых'
        res['response']['buttons'][3]['hide'] = True                      
        res['response']['buttons'][4]['title'] = 'Поп'
        res['response']['buttons'][4]['hide'] = True
        res['response']['buttons'][5]['title'] = 'Аниме'
        res['response']['buttons'][5]['hide'] = True     
        res['response']['buttons'][6]['title'] = 'Сменить сложность'
        res['response']['buttons'][6]['hide'] = True            
        return sessionStorage

    elif sessionStorage['menu'] < 6:
        res['response']['text'] = 'Не понимаю что вы имеете в виду, попробуйте оветить более понятней. Остаемся в этой категории или выберем другую?'
        res['response']['buttons'] = [{},{},{}]
        res['response']['buttons'][0]['title'] = 'Остаемся'
        res['response']['buttons'][0]['hide'] = True
        res['response']['buttons'][1]['title'] = 'Сменить категорию'
        res['response']['buttons'][1]['hide'] = True
        res['response']['buttons'][2]['title'] = 'Сменить сложность'
        res['response']['buttons'][2]['hide'] = True  
        return


if __name__ == "__main__":
    app.run(debug=True)